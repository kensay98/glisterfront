import $ from 'jquery'
import 'slick-carousel'

// $(document).ready(function(){
//     console.log(`op!`)
//     $('.care-product').slick();
// });
// Это для того, чтобы скрывать разные блоки в cms
// window.type = 'abo';

$(document).ready(function () {
  if (window.type && window.type === 'abo') {
    $('.only-abo').removeClass('only-abo');
  }
  $(".accordion h3:first").addClass("active");
  $(".accordion p:not(:first)").hide();
  $(".accordion h3").click(function () {
    $(this).next("p").slideToggle("slow")
      .siblings("p:visible").slideUp("slow");
    $(this).toggleClass("active");
    $(this).siblings("h3").removeClass("active");
  });
});

$(document).ready(() => {

  $('.brush-operation-modes-slider').slick({
    infinite: false,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true,
    arrows: false,
    touchThreshold: 10,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: true
        }
      }]
  })

  $('.care-slider').slick({
    infinite: false,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true,
    arrows: false,
    touchThreshold: 10,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: true
        }
      }]
  })

  $('.recommendations-slider').slick({
    infinite: false,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true,
    arrows: false,
    touchThreshold: 10,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: true
        }
      }]
  })

  $('.right-care-slider').slick({
    infinite: false,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true,
    arrows: false,
    touchThreshold: 10,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: true
        }
      }]
  })


  var $apeearBlocks = $('.js-appear');
  var windowHeight = $(window).height();
  var $scrolTo = $('.js-scroll-to');

  $scrolTo.on('click', function (e) {
    e.preventDefault();
    var thisId = $(this).attr('href');
    scrollTo($(thisId), 80);
  });

  /**
   * scroll to block
   * @param selector
   */
  scrollTo = function (selector, offset, scrollElem, position) {
    setTimeout(function () {
      !offset ? offset = 0 : offset;
      if (!position == true) {
        var scroll = $(selector).offset().top - offset;
      } else {
        var scroll = $(selector).position().top - offset;
      }

      if (!scrollElem) {
        scrollElem = $('html,body');
      }
      scrollElem.animate({
        scrollTop: scroll
      }, 500);
    }, 10);
  };

  function showAnim() {
    var scrollTop = $(window).scrollTop();
    $apeearBlocks.each(function () {
      var valTop = $(this).offset().top;

      if (scrollTop - valTop >= parseInt(windowHeight - windowHeight / 5) * -1 && !$(this).hasClass('show')) {
        $(this).addClass('show');
      }

    })
  }

  showAnim();
  if (window.innerWidth <= 480 && $('#layout')) {
    $('#layout').on('scroll', function () {
      windowHeight = $(window).height();
      showAnim();
    });
  } else {
    $(window).on('scroll', function () {
      windowHeight = $(window).height();
      showAnim();
    });
  }
})